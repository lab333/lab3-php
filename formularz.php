
<!DOCTYPE HTML>
<html lang="pl">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="style.css" type="text/css">
  </head>
  <body>

    <header>
      <div id="logo">ANKIETA</div>
    </header>


<form action="dodaj_do_bazy.php" method="post" accept-charset="ISO-8859-1">

<h1>1. Jak często korzystasz z portali internetowych</h1><br>
<label class="container">10 minut dzienne
  <input type ="radio" name="1" value="1">
  <span class="checkmark"></span>
</label>
  <label class="container">30 minut dzienne
  <input type ="radio" name="1" value="2">
  <span class="checkmark"></span>
</label>
  <label class="container" >1h dzienne
  <input type ="radio" name="1" value="3">
  <span class="checkmark"></span>
</label>
  <label class="container">2-5h  dzienne
  <input type ="radio" name="1" value="4">
  <span class="checkmark"></span>
</label>
  <label class="container">więcej niz 5h dzienne
  <input type ="radio" name="1" value="5">
  <span class="checkmark"></span>
</label>
  <label class="container">kilka razy w tygodniu
  <input type ="radio" name="1" value="6">
  <span class="checkmark"></span>
</label>
  <label class="container">kilka razy w miesiącu
  <input type ="radio" name="1" value="7">
  <span class="checkmark"></span>
</label>
  <label class="container">kilka razy w roku
  <input type ="radio" name="1" value="8">
  <span class="checkmark"></span>
</label>
  <label class="container">w ogóle nie korzystam
  <input type ="radio" name="1" value="9">
  <span class="checkmark"></span>
</label>
<br>



<h1>2. Z jakich portali społecznościowych korzystasz najczęściej?</h1><br>
  <label class="container">facebook

  <input type ="radio" name="2" value="1">
  <span class="checkmark"></span>
</label>
  <label class="container">google+
  <input type ="radio" name="2" value="2">
  <span class="checkmark"></span>
</label>
  <label class="container">myspace
  <input type ="radio" name="2" value="3">
  <span class="checkmark"></span>
</label>
  <label class="container">nasza-klasa
  <input type ="radio" name="2" value="4">
  <span class="checkmark"></span>
</label>
  <label class="container">grono
  <input type ="radio" name="2" value="5">
  <span class="checkmark"></span>
</label>
  <label class="container">linked in
  <input type ="radio" name="2" value="6">
  <span class="checkmark"></span>
</label>
  <label class="container">goldenline
  <input type ="radio" name="2" value="7">
  <span class="checkmark"></span>
</label>
  <label class="container">youtube
  <input type ="radio" name="2" value="8">
  <span class="checkmark"></span>
</label>
  <label class="container">vimeo
  <input type ="radio" name="2" value="9">
  <span class="checkmark"></span>
</label>
  <label class="container">picassa
  <input type ="radio" name="2" value="10">
  <span class="checkmark"></span>
</label>
  <label class="container">pinterest
  <input type ="radio" name="2" value="11">
  <span class="checkmark"></span>
</label>
  <label class="container">inne
  <input type ="radio" name="2" value="12">
  <span class="checkmark"></span>
</label>
<br>


<h1>3. W jakim celu korzystasz z portali społecznościowych?</h1><br>
  <label class="container">kontakt ze znajomymi
  <input type ="radio" name="3" value="1">
  <span class="checkmark"></span>
</label>
  <label class="container">poznawanie nowych ludzi
  <input type ="radio" name="3" value="2">
  <span class="checkmark"></span>
</label>
  <label class="container">zdobywanie kontaktów biznesowych
  <input type ="radio" name="3" value="3">
  <span class="checkmark"></span>
</label>
  <label class="container">wymiana opinii, poglądów
  <input type ="radio" name="3" value="4">
  <span class="checkmark"></span>
</label>
  <label class="container">wymiana informacji edukacyjnych, prowadzenie dyskusji naukowych
  <input type ="radio" name="3" value="5">
  <span class="checkmark"></span>
</label>
  <label class="container">wymiana materiałów edukacyjnych
  <input type ="radio" name="3" value="6">
  <span class="checkmark"></span>
</label>
  <label class="container">łączenie się w grupy zainteresowań
  <input type ="radio" name="3" value="7">
  <span class="checkmark"></span>
</label>
  <label class="container">zdobywanie informacji o wydarzeniach kulturowych
  <input type ="radio" name="3" value="8">
  <span class="checkmark"></span>
</label>
  <label class="container">zdobywanie informacji o wydarzeniach edukacyjnych
  <input type ="radio" name="3" value="9">
  <span class="checkmark"></span>
</label>
  <label class="container">zdobywanie informacji o wydarzeniach biznesowych
  <input type ="radio" name="3" value="10">
  <span class="checkmark"></span>
</label>
  <label class="container">korzystanie z aplikacji wspomagających naukę
  <input type ="radio" name="3" value="11">
  <span class="checkmark"></span>
</label>
  <label class="container">komunikowanie się z innymi użytkownikami poprzez wiadomości tekstowe/chat
  <input type ="radio" name="3" value="12">
  <span class="checkmark"></span>
</label>
  <label class="container">możliwość planowania i zarządzania projektami
  <input type ="radio" name="3" value="13">
  <span class="checkmark"></span>
</label>
  <label class="container">promowaniu swoich produktów lub usług
  <input type ="radio" name="3" value="14">
  <span class="checkmark"></span>
</label>
  <label class="container">wyłącznie do pracy
  <input type ="radio" name="3" value="15">
  <span class="checkmark"></span>
</label>
<br>

<h1>4. Wskaż najważniejsze cechy portalu społecznościowego. Wskaż minimum 4 cechy</h1><br>
  <label class="container">atrakcyjny interfejs
  <input type ="radio" name="4" value="1">
  <span class="checkmark"></span>
  </label>
  <label class="container">łatwy w obsłudze interfejs
  <input type ="radio" name="4" value="2">
  <span class="checkmark"></span>
  </label>
  <label class="container">integracja z facebookiem
  <input type ="radio" name="4" value="3">
  <span class="checkmark"></span>
  </label>
  <label class="container">zaawansowana funkcjonalność serwisu
  <input type ="radio" name="4" value="4">
  <span class="checkmark"></span>
  </label>
  <label class="container">aplikacja mobilna serwisu
  <input type ="radio" name="4" value="5">
  <span class="checkmark"></span>
  </label>
  <label class="container">duża społeczność
  <input type ="radio" name="4" value="6">
  <span class="checkmark"></span>
  </label>
  <label class="container">ciekawa nazwa
  <input type ="radio" name="4" value="7">
  <span class="checkmark"></span>
  </label>
  <label class="container">nieirytujące reklamy
  <input type ="radio" name="4" value="8">
  <span class="checkmark"></span>
  </label>
  <label class="container">brak opłat za korzystanie z serwisu
  <input type ="radio" name="4" value="9">
  <span class="checkmark"></span>
  </label>
  <label class="container">gry społecznościowe
  <input type ="radio" name="4" value="10">
  <span class="checkmark"></span>
  </label>
<br>

  <h1>5.Jeśli korzystasz z portali społecznościowych to jakich narzędzi używasz?</h1><br>
  <label class="container">kalendarz
  <input type ="radio" name="5" value="1">
  <span class="checkmark"></span>
  </label>
  <label class="container">wymiana plików
  <input type ="radio" name="5" value="2">
  <span class="checkmark"></span>
  </label>
  <label class="container">wymiana zdjęć/filmów
  <input type ="radio" name="5" value="3">
  <span class="checkmark"></span>
  </label>
  <label class="container">mail/mail grupowy
  <input type ="radio" name="5" value="4">
  <span class="checkmark"></span>
  </label>
  <label class="container">forum
  <input type ="radio" name="5" value="5">
  <span class="checkmark"></span>
  </label>
  <label class="container"> wyszukiwarka
  <input type ="radio" name="5" value="6">
  <span class="checkmark"></span>
  </label>
  <label class="container"> chat grupowy
  <input type ="radio" name="5" value="7">
  <span class="checkmark"></span>
  </label>
  <label class="container"> tworzenie grup użytkowników
  <input type ="radio" name="5" value="8">
  <span class="checkmark"></span>
  </label>
  <label class="container"> wiadomości prywatne/wiadomości grupowe
  <input type ="radio" name="5" value="9">
  <span class="checkmark"></span>
  </label>
  <label class="container"> posty, dodawanie komentarzy
  <input type ="radio" name="5" value="10">
  <span class="checkmark"></span>
  </label>
  <label class="container"> aplikacje społecznościowe wspomagające naukę i pracę
  <input type ="radio" name="5" value="11">
  <span class="checkmark"></span>
  </label>
  <label class="container"> e-learningi
  <input type ="radio" name="5" value="12">
  <span class="checkmark"></span>
  </label>
<br>


<h1>6.Czy korzystałbyś/łabyś z portalu społecznościowego, skupiającego wszystkie niezbędne narzędzia internetowe, m.in. te, które znajdują się w pyt.5?</h1><br>
  <label class="container"> na pewno! Jest to super pomysł!
  <input type ="radio" name="6" value="1">
  <span class="checkmark"></span>
  </label>
  <label class="container"> jeśli będzie dobrze zrobiony, to raczej tak
  <input type ="radio" name="6" value="2">
  <span class="checkmark"></span>
  </label>
  <label class="container"> trudno powiedzieć
  <input type ="radio" name="6" value="3">
  <span class="checkmark"></span>
  </label>
  <label class="container"> raczej nie
  <input type ="radio" name="6" value="4">
  <span class="checkmark"></span>
  </label>
  <label class="container"> na pewno nie! Nie potrzebuję zmian
  <input type ="radio" name="6" value="5">
  <span class="checkmark"></span>
  </label>
<br>

<h1>7.Jakie elementy reklamy zachęcają Cię do korzystania z portali społecznościowych?</h1><br>
  <label class="container"> konkursy z nagrodami
  <input type ="radio" name="7" value="1">
  <span class="checkmark"></span>
  </label>
  <label class="container"> reklamy kontekstowe
  <input type ="radio" name="7" value="2">
  <span class="checkmark"></span>
  </label>
  <label class="container"> zniżki i rabaty dedykowane dla użytkowników portalu społecznościowego
  <input type ="radio" name="7" value="3">
  <span class="checkmark"></span>
  </label>
  <label class="container"> organizowanie imprez/happeningów dla użytkowników portalu społecznościowego
  <input type ="radio" name="7" value="4">
  <span class="checkmark"></span>
  </label>
  <label class="container"> gadżety, upominki
  <input type ="radio" name="7" value="5">
  <span class="checkmark"></span>
  </label>
<br>

<h1>8.Płeć</h1><br>
  <label class="container"> kobieta
  <input type ="radio" name="8" value="1">
  <span class="checkmark"></span>
  </label>
  <label class="container"> mężczyzna
  <input type ="radio" name="8" value="2">
  <span class="checkmark"></span>
  </label>
<br>


<h1>9.Wiek</h1><br>
  <label class="container"> 12 - 15 lat
  <input type ="radio" name="9" value="1">
  <span class="checkmark"></span>
  </label>
  <label class="container"> 16 - 19 lat
  <input type ="radio" name="9" value="2">
  <span class="checkmark"></span>
  </label>
  <label class="container"> 20 - 25 lat
  <input type ="radio" name="9" value="3">
  <span class="checkmark"></span>
  </label>
  <label class="container"> 25 - 30 lat
  <input type ="radio" name="9" value="4">
  <span class="checkmark"></span>
  </label>
<br>
<h1>10.Wykształcenie</h1><br>
  <label class="container"> jestem uczniem szkoły podstawowej
  <input type ="radio" name="10" value="1">
  <span class="checkmark"></span>
  </label>
  <label class="container"> jestem uczniem gimnazjum
  <input type ="radio" name="10" value="2">
  <span class="checkmark"></span>
  </label>
  <label class="container"> jestem uczniem szkoły średniej
  <input type ="radio" name="10" value="3">
  <span class="checkmark"></span>
  </label>
  <label class="container"> jestem studentem
  <input type ="radio" name="10" value="4">
  <span class="checkmark"></span>
  </label>
  <label class="container"> ukończyłem edukację
  <input type ="radio" name="10" value="5">
  <span class="checkmark"></span>
  </label>
<br>

    <input class="inputsubmit" type="submit" name ="wyslij" value="Wyślij wiadomość"  />
</form>
  </body>
