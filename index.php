<!doctype html>
<html class="h-full">
  <head>
  <meta charset="UTF-8">
  <title>Ankieta</title>
  <link href="style.css" rel="stylesheet">
  </head>
  <body class="flex flex-col h-full">

<?php
      session_start();
      header('Content-Type: text/html; charset=utf-8');
      include ('db.php');

      $Tabela = "CREATE TABLE IF NOT EXISTS dane_ankieta (
        `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
        `pytanie_1` VARCHAR(100) NOT NULL,
        `pytanie_2` VARCHAR(100) NOT NULL,
        `pytanie_3` VARCHAR(100) NOT NULL,
        `pytanie_4` VARCHAR(100) NOT NULL,
        `pytanie_5` VARCHAR(100) NOT NULL,
        `pytanie_6` VARCHAR(100) NOT NULL,
        `pytanie_7` VARCHAR(100) NOT NULL,
        `pytanie_8` VARCHAR(100) NOT NULL,
        `pytanie_9` VARCHAR(100) NOT NULL,
        `pytanie_10` VARCHAR(100) NOT NULL,

        PRIMARY KEY (`id`)
        )";

        mysqli_query($connect, $Tabela);

      include ('formularz.php');


      $connect->close();
?>



</body>
</html>
